class manager():
    def callfizzbuzz(self):
        fizzbuzzfunction = [
            FizzFizzBuzzBuzz(),
            FizzBuzz(),
            FizzFizz(),
            BuzzBuzz(),
            Fizz(),
            Buzz(),
            NoFizzBuzz(),
        ]
        return fizzfizzbuzzbuzz(fizzbuzzfunction)


class fizzfizzbuzzbuzz():
    def __init__(self, fizzbuzzfunction):
        self.fizzbuzzfunction = fizzbuzzfunction

    def result(self, n):
        for Condition in self.fizzbuzzfunction:
            if Condition.is_manage(n):
                return Condition.result(n)


class Condition():

    def is_manage(self, num):
        pass

    def result(self, num):
        pass


class NoFizzBuzz(Condition):

    def is_manage(self, n):
        return True

    def result(self, n):
        return "NoFizzBuzz"


class FizzBuzz(Condition):

    def is_manage(self, n):
        return n % 3 == 0 and n % 5 == 0

    def result(self, n):
        return "FizzBuzz"


class Fizz(Condition):

    def is_manage(self, n):
        return n % 3 == 0

    def result(self, n):
        return "Fizz"


class Buzz(Condition):

    def is_manage(self, n):
        return n % 5 == 0

    def result(self, n):
        return "Buzz"


class FizzFizz(Condition):

    def is_manage(self, n):
        return n % 9 == 0

    def result(self, n):
        return "FizzFizz"


class BuzzBuzz(Condition):

    def is_manage(self, n):
        return n % 25 == 0

    def result(self, n):
        return "BuzzBuzz"


class FizzFizzBuzzBuzz(Condition):

    def is_manage(self, n):
        return n % 9 == 0 and n % 25 == 0

    def result(self, n):
        return "FizzFizzBuzzBuzz"


'''
n = int(input("Input number : "))
if n in range(10000):
    result = manager().callfizzbuzz()
    print(result.result(n))
'''
